﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class QuestionScript : MonoBehaviour
{
    public static string strQuestionType = "DATADELETE";
    [SerializeField] private Text Question;

    static GameObject target;
    static string strComponentName;
    static string strMethodName;

    // Start is called before the first frame update
    void Start()
    {
        SetQuestion();
    }

    
    public void SetQuestion()
    {
        if(strQuestionType == "DATADELETE")
        {
            Question.text = "순위 정보를 <color=red>삭제</color>하시겠습니까?";
            target = GameObject.Find("DataMgr");
            strComponentName = "DataManager";
            strMethodName = "DeleteData";
        }
    }
    public void Submit()
    {
        target.GetComponent(strComponentName).SendMessage(strMethodName);
    }
}
