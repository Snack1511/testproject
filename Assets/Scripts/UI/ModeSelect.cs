using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Custom;
public class ModeSelect : MonoBehaviour
{

    //버튼 입력시 호출
    public void ClickNormal()
    {
        GameManager.Instance.SetMode(ModeData.NORMAL);
    }
    public void Click100Stones()
    {
        GameManager.Instance.SetMode(ModeData.STONES100);
    }

}


