﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Custom;
public class NoticeScript : MonoBehaviour
{
    public static string NoticeTag;
    [SerializeField] private Text Title;
    [SerializeField] private Text Context;
    [SerializeField] private Sprite[] srcImg;
    [SerializeField] private GameObject PageMoveBtn;
    [SerializeField] private GameObject Images;

    int iPageidx;
    int MaxPage;
    NoticeData[] Notice = new NoticeData[3];

    // Start is called before the first frame update
    
    void Start()
    {
        iPageidx = 0;
        SetNoticeData(NoticeTag);
        VisibleBtns(MaxPage);
        PrintPage(iPageidx);
    }
    //좌측버튼이 호출
    public void ClickPrev()
    {
        if(iPageidx > 0)
        {
            iPageidx--;
            VisiblePMoveBtn(PageMoveBtn.transform.GetChild(1).gameObject, true);
            if(iPageidx <= 0)
            {
                VisiblePMoveBtn(PageMoveBtn.transform.GetChild(0).gameObject, false);
            }
        }
        PrintPage(iPageidx);
    }
    //우측버튼이 호출
    public void ClickNext()
    {
        if (iPageidx < MaxPage - 1)
        {
            iPageidx++;
            VisiblePMoveBtn(PageMoveBtn.transform.GetChild(0).gameObject, true);
            if (MaxPage > 1)
            {
                if (iPageidx >= MaxPage - 1)
                {
                    VisiblePMoveBtn(PageMoveBtn.transform.GetChild(1).gameObject, false);
                }
            }
        }
        PrintPage(iPageidx);
    }
    //태그따라 불러올 안내 데이터 변경
    public void SetNoticeData(string tag)
    {
        if(tag == "Developer")
        {
            MaxPage = 1;
            iPageidx = 0;
            Notice[0] = NoticeData.DeveloperInfo;
        }
        else if(tag == "Normal")
        {
            MaxPage = NoticeData.NormalInfo.Length;
            for (int i = 0; i < MaxPage; i++)
            {
                Notice[i] = NoticeData.NormalInfo[i];
            }
        }
        else if(tag == "Stone100")
        {
            MaxPage = NoticeData.Stone100Info.Length;
            for (int i = 0; i < NoticeData.Stone100Info.Length; i++)
            {
                Notice[i] = NoticeData.Stone100Info[i];
            }
        }
        else if(tag == "Guide")
        {
            MaxPage = 1;
            iPageidx = 0;
            Notice[0] = NoticeData.GameGuide;
        }
        else
        {
#if UNITY_EDITOR
            Debug.Log("Please select tag");
#endif
        }

    }
    
    //페이지 업데이트 함수
    void PrintPage(int n)
    {
        if (Notice[n].HAVEIMG)
        {
            Context.rectTransform.anchorMax = new Vector2(1, 0.34f);
            Images.SetActive(true);
            VisibleImage(Notice[n].IMGCNT);
        }
        else
        {
            Images.SetActive(false);
            Context.rectTransform.anchorMax = new Vector2(1, 0.685f);
        }
        Title.text = Notice[n].TITLE;
        Context.text = Notice[n].CONTEXT;
    }

    //여러페이지가 존재하는 안내데이터의 경우 페이지 로드시 버튼 활성화
    void VisibleBtns(int n)
    {
        if (n <= 1)
        {
            PageMoveBtn.SetActive(false);
        }
        else
        {
            PageMoveBtn.SetActive(true);
            ControlPMoveBtn(iPageidx, MaxPage);
        }
    }
    //페이지 버튼 컨트롤
    void ControlPMoveBtn(int idx, int maxpage)
    {
        if (idx <= 0)
        {
            VisiblePMoveBtn(PageMoveBtn.transform.GetChild(0).gameObject, false);
            VisiblePMoveBtn(PageMoveBtn.transform.GetChild(1).gameObject, true);
        }
        else if (idx >= maxpage - 1)
        {
            VisiblePMoveBtn(PageMoveBtn.transform.GetChild(0).gameObject, true);
            VisiblePMoveBtn(PageMoveBtn.transform.GetChild(1).gameObject, false);
        }
        else
        {
            VisiblePMoveBtn(PageMoveBtn.transform.GetChild(0).gameObject, true);
            VisiblePMoveBtn(PageMoveBtn.transform.GetChild(1).gameObject, true);
        }
    }
    //페이지의 양 끝일 경우 해당 방향의 버튼 비활성화
    void VisiblePMoveBtn(GameObject obj, bool flg)
    {
        if (obj.activeSelf != flg)
        {
            obj.SetActive(flg);
        }
    }

    
    //이미지가 존재하는 페이지의 경우 이미지 활성화
    void VisibleImage(int Imagecnt)
    {
        if (Imagecnt == 1)
        {
            SetImageVisible(Images.transform, true);
            SetImage(srcImg[getImgidx(NoticeTag)[iPageidx-1]] , 0);

        }
        else if (Imagecnt == 2)
        {
            SetImageVisible(Images.transform);
            SetImage(srcImg[getImgidx(NoticeTag)[0]], 0);
            SetImage(srcImg[getImgidx(NoticeTag)[1]], 1);
        }

    }

    //이미지 활성화
    void SetImageVisible(Transform parent, bool flg)
    {
        if (parent.GetChild(1).gameObject.activeSelf)
        {
            parent.GetChild(1).gameObject.SetActive(!flg);
        }
        if (!parent.GetChild(0).gameObject.activeSelf)
        {
            parent.GetChild(0).gameObject.SetActive(flg);
        }
    }
    void SetImageVisible(Transform parent)
    {
        parent.GetChild(0).gameObject.SetActive(true);
        parent.GetChild(1).gameObject.SetActive(true);
    }

    //이미지 할당 함수
    void SetImage(Sprite imgsrc, int n = 0)
    {
        SetImagesize(Images.transform.GetChild(n).GetComponent<Image>(), Images, imgsrc);
    }
    
    //이미지 사이즈 변경 함수
    void SetImagesize(Image img, GameObject parent, Sprite imgsrc)
    {
        parent.GetComponent<HorizontalLayoutGroup>().childControlHeight = false;
        img.GetComponent<AspectRatioFitter>().aspectMode = AspectRatioFitter.AspectMode.None;
        img.sprite = imgsrc;
        img.SetNativeSize();
        float ratio = imgsrc.rect.width / imgsrc.rect.height;
        img.GetComponent<AspectRatioFitter>().aspectMode = AspectRatioFitter.AspectMode.HeightControlsWidth;
        img.GetComponent<AspectRatioFitter>().aspectRatio = ratio;
        parent.GetComponent<HorizontalLayoutGroup>().childControlHeight = true;
    }

    //이미지 인덱스 번호를 참조하는 함수
    int[] getImgidx(string tag)
    {
        int[] n;
        if (tag == "Normal")
        {
            n = new int[2]{ 0, 1 };
        }
        else if (tag == "Stone100")
        {
            n = new int[2] { 2, 3 };
        }
        else if (tag == "Guide")
        {
            n = new int[2] { 4, 5 };
        }
        else
        {
#if UNITY_EDITOR
            Debug.Log("Please select tag");
#endif
            n = new int[1];
            
        }//순서중요
        return n;
    }

    
}


