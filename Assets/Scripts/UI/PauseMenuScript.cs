﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Doozy.Engine;
using Doozy.Engine.UI;
using Doozy.Engine.Soundy;
using UnityEngine.Audio;
using UnityEngine.UI;
public class PauseMenuScript : MonoBehaviour
{
    //GameObject Sliders;
    [SerializeField] private Slider MasterSlider;
    [SerializeField] private Slider UISlider;
    [SerializeField] private Slider BGMSlider;
    [SerializeField] private AudioMixer mixer;

    UIPopup popup;
    // Start is called before the first frame update
    void Start()
    {
        popup = GetComponent<UIPopup>();
        SetSlider(mixer);
    }

    //볼륨조절 함수
    public void SetMasterVolume(float _value)
    {
        if(mixer.SetFloat("MasterVolume", _value))
        {
#if UNITY_EDITOR
            Debug.Log("Control Complete");
#endif
        }
    }
    public void SetUIVolume(float _value)
    {
        if (mixer.SetFloat("UIVolume", _value))
        {
#if UNITY_EDITOR
            Debug.Log("Control Complete");
#endif
        }
    }
    public void SetBGMVolume(float _value)
    {
        if (mixer.SetFloat("BGMVolume", _value))
        {
#if UNITY_EDITOR
            Debug.Log("Control Complete");
#endif
        }
    }

    void SetSlider(AudioMixer mix)
    {
        float _value;
        mix.GetFloat("MasterVolume", out _value);
        MasterSlider.value = _value;
        mix.GetFloat("BGMVolume", out _value);
        BGMSlider.value = _value;
        mix.GetFloat("UIVolume", out _value);
        UISlider.value = _value;
    }
}
