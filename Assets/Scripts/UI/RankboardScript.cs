﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class RankboardScript : MonoBehaviour
{
    public static string strRankType;
    [SerializeField] private GameObject imgScore;
    [SerializeField] private GameObject imgTime;
    [SerializeField] private Text txtFirst;
    [SerializeField] private Text txtSecond;
    [SerializeField] private Text txtThird;
    // Start is called before the first frame update
    void Start()
    {
        GetRanking();
    }
    
    void GetRanking()
    {
        
        if (strRankType == "SCORE") {
            var Value = GameManager.Instance.GetScoreRank();
            imgScore.SetActive(true);
            imgTime.SetActive(false);
            txtFirst.text = Value[0].ToString();
            txtSecond.text = Value[1].ToString();
            txtThird.text = Value[2].ToString();
        }
        else if(strRankType == "TIME")
        {
            var Value = GameManager.Instance.GetTimeRank();
            imgScore.SetActive(false);
            imgTime.SetActive(true);
            txtFirst.text = Value[0].ToString("N2");
            txtSecond.text = Value[1].ToString("N2");
            txtThird.text = Value[2].ToString("N2");
        }
        else
        {
            Debug.Log("incorrect Ranktype");
        }
    }
    


}
