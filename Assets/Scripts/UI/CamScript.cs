﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Doozy.Engine;
public class CamScript : MonoBehaviour
{
    bool bGameStart;
    Camera cam;

    #region UnityMethod
    // Start is called before the first frame update
    void Start()
    {
        cam = GetComponent<Camera>();
        bGameStart = false;
        var gameMgr = GameManager.Instance;
        gameMgr.GameStartEvent.AddListener(ZoomIn);
        gameMgr.GameResetEvent.AddListener(ZoomOut);
    }

    // Update is called once per frame
    void Update()
    {
        if (!bGameStart)
        {
            ZoomOutCamEffect();
        }
        else
        {
            ZoominCamEffect();
        }
    }
    #endregion

    void ZoominCamEffect()
    {
        if (cam.fieldOfView > 35)
        {
            cam.fieldOfView -= Time.fixedDeltaTime * 100;
        }
        else if(cam.fieldOfView < 35)
        {
            cam.fieldOfView = 35;
        }
    }
    void ZoomOutCamEffect()
    {
        if (cam.fieldOfView < 50)
        {
            cam.fieldOfView += Time.fixedDeltaTime * 100;
        }
        else if (cam.fieldOfView > 50)
        {
            cam.fieldOfView = 50;
        }
    }

    #region GameEventListener 동작
    /// <summary>
    /// <see cref="GameManager.GameStartEvent"/>에서 호출
    /// </summary>
    public void ZoomIn()
    {
        bGameStart = true;
    }
    /// <summary>
    /// <see cref="GameManager.GameResetEvent"/>에서 호출
    /// </summary>
    public void ZoomOut()
    {
        bGameStart = false;
    }
    #endregion
}
