﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class PlayerScript : MonoBehaviour
{
    [SerializeField] private int AttTag;
    [SerializeField] private bool bClick;
    private Animator anim;
    private SpriteRenderer Render;
    public int TAG
    {
        get { return AttTag; }
    }
    public bool CLICK
    {
        get { return bClick; }
    }
    // Start is called before the first frame update
    #region UnityMethod
    void Start()
    {
        anim = GetComponent<Animator>();
        Render = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            Click_Btn1();
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            Click_Btn2();
        }
#endif
    }
    #endregion
    //클릭해제
    public void ReleaseClick() {
        bClick = false;
    }
    //왼쪽버튼 클릭
    public void Click_Btn1()
    {
        //Debug.Log("Click1");
        PlayAnima();
        bClick = true;
        AttTag = 1;
    }
    //오른쪽버튼클릭
    public void Click_Btn2()
    {
#if UNITY_EDITOR
        Debug.Log("Click2");
#endif
        PlayAnima();
        bClick = true;
        AttTag = 2;
    }
    //플레이어 애니메이션 재생
    void PlayAnima()
    {
        anim.Play("Player_Att");
    }
}
