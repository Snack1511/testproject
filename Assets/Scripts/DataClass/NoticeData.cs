﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//NoticePopup정보만 불러오는대 사용
namespace Custom
{
    public class NoticeData
    {
        public static NoticeData DeveloperInfo = new NoticeData("제작자 정보", MergeSentence("제작자 : <size=60>최민제</size>", "이-메일 : cmj6983", "     @gmail.com"));
        public static NoticeData[] NormalInfo = {new NoticeData("노-말", MergeSentence("미션 : 최대한 <color=red>많이</color> 쳐내자", "클릭 실수 : <color=red>X</color>", "요구 블럭수 : 0")),
                                              new NoticeData("타이-머", MergeSentence("남은 <color=red>시간</color>을 표시", "100초동안 점차 감소", "", 2), 1, true),
                                              new NoticeData("스코-어", MergeSentence("획득한 <color=red>점수</color>를 표시", "", "", 1), 1, true) };
        public static NoticeData[] Stone100Info = {new NoticeData("100-STONE", MergeSentence("미션 : 최대한 <color=red>빨리</color> 쳐내자", "클릭 실수 : 허용", "요구 블럭수 : 100")),
                                                new NoticeData("골-미터", MergeSentence("블럭을 제거할때마다", "점차 <color=red>우측으로 이동</color>", "", 2), 1, true),
                                                new NoticeData("콤보-미터", MergeSentence("<color=red>5초 이내</color>에 블럭제거 시", "미터 초기화 및 <color=red>시간 보너스</color>", "", 2), 1, true)};
        public static NoticeData GameGuide = new NoticeData("조작방법", MergeSentence("블럭의 모양과 <color=red>같은 버튼</color> 터치", "", "", 1), 2, true);

        string strTitle;
        string strContext;
        int iImgcnt;
        bool bImg;

        public string TITLE
        {
            get { return strTitle; }
        }
        public string CONTEXT
        {
            get { return strContext; }
        }
        public int IMGCNT
        {
            get { return iImgcnt; }
        }
        public bool HAVEIMG
        {
            get { return bImg; }
        }

        //문자열 병합
        public static string MergeSentence(string s1, string s2, string s3, int n = 3)
        {
            string Result;
            if (n == 1)
            {
                Result = s1;
            }
            else if (n == 2)
            {
                Result = s1 + "\n" + s2;
            }
            else if (n == 3)
            {
                Result = s1 + "\n" + s2 + "\n" + s3;
            }
            else
            {
                Result = "내용내용내용내용내용내용내용내용내용내용내용내용";
            }
            return Result;
        }

        NoticeData(string title, string context, int imgcnt = 0, bool haveImg = false)
        {
            strTitle = title;
            strContext = context;
            iImgcnt = imgcnt;
            bImg = haveImg;
        }


    }
}
