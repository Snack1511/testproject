﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//블럭정보 클래스
namespace Custom
{
    public class BlockData : MonoBehaviour
    {

        int iTag;
        int iScore;
        bool bObstacle;

        public int TAG { get { return iTag; } }
        public int SCORE { get { return iScore; } }
        public bool OBSTACLE { get { return bObstacle; } }

        //생성자
        public BlockData(int _Tag = 0, int _Score = 0, bool _Obstacle = false)
        {
            iTag = _Tag;
            iScore = _Score;
            bObstacle = _Obstacle;
        }
        //블럭 데이터 재설정
        public void ResetData(int tagid = 0, bool _Obstacle = false)
        {
            iTag = tagid;
            iScore = tagid;
            bObstacle = _Obstacle;
            if (bObstacle)
            {
                iScore *= 2;
            }
        }

        #region Debug
        public void DebugData()
        {
            Debug.Log("Tag : " + iTag);
            Debug.Log("Score : " + iScore);
            Debug.Log("Obstacle : " + bObstacle);
        }
        #endregion
    }
}