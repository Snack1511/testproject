﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Custom
{
    [System.Serializable]
    ///<remarks> 구조체로 호출할 경우 시리얼라이즈 안됨</remarks>>
    public class AchievementContent
    {
        bool bAchieve;
        string strAchieveSenetence;
        int oConditon;

        public string STRING
        {
            get { return strAchieveSenetence; }
        }
        public bool ISACHIEVE
        {
            get { return bAchieve; }
        }
        public int CONDITION
        {
            get { return oConditon; }
        }

        public AchievementContent(string str = "", int condition = 0, bool flg = false)
        {
            strAchieveSenetence = str;

            oConditon = condition;
            bAchieve = flg;
        }
        //해당 과제 달성 설정함수
        public void SetAchieve(int condition, bool flg)
        {
            if (bAchieve != flg)
            {
                if (oConditon == condition)
                {
                    bAchieve = flg;
                }
            }
        }

    }
}