﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Custom
{
    [System.Serializable]
    public class ModeData
    {
        public static ModeData NORMAL = new ModeData(100, 0, 1);
        public static ModeData STONES100 = new ModeData(0, 100, 0);

        [SerializeField]float fPlayTime;
        [SerializeField]int iMissCount;
        [SerializeField]int iGoalCount;

        public float TIME { get { return fPlayTime; } }
        public int MISS { get { return iMissCount; } }
        public int GOAL { get { return iGoalCount; } }

        public ModeData(float Time = 0, int Goal = 0, int Count = 0)
        {
            fPlayTime = Time;
            iGoalCount = Goal;
            iMissCount = Count;

        }
        #region Debug
        public void DebugModeData()
        {
            Debug.Log("Time : " + fPlayTime + "Goal : " + iGoalCount + "Miss : " + iMissCount);
        }
        #endregion
    }
}