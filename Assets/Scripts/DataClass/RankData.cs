﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Custom
{
    [System.Serializable]
    public class RankData
    {
        int[] iarrScoreRank;
        float[] farrTimeRank;

        public int[] SCORERANK
        {
            get { return iarrScoreRank; }
        }
        public float[] TIMERANK
        {
            get { return farrTimeRank; }
        }

        public static bool CheckData<T>(T a, T b)
        {
            object obj1 = a;
            object obj2 = b;

            if (a.GetType() == typeof(float))
            {
                if ((float)obj1 < (float)obj2)
                {
                    return true;
                }
                else { return false; }
            }
            else
            {
                if ((int)obj1 > (int)obj2)
                {
                    return true;
                }
                else { return false; }
            }

        }

        //생성자
        public RankData(int i, float f)
        {
            iarrScoreRank = new int[3] { i, i, i };
            farrTimeRank = new float[3] { f, f, f };
        }
        /// <summary>
        /// <para/> 받은 변수를 <see cref="CheckRank{T}(T, int[], float[])"/>에서 비교한 뒤 배열 반환 
        /// <para/> 반환된 배열을 <see cref="Convert"/>로 변환시킨후 int배열에 저장
        /// </summary>
        /// <param name="_currentScore">현재 GameManager의 점수 할당</param>
        public void SetBestScore(int _currentScore)
        {
            iarrScoreRank = Convert<int>(CheckRank(_currentScore, iarrScoreRank, farrTimeRank), 3);
        }

        /// <summary>
        /// <para/> 받은 변수를 <see cref="CheckRank{T}(T, int[], float[])"/>에서 비교한 뒤 배열 반환 
        /// <para/> 반환된 배열을 <see cref="Convert"/>로 변환시킨후 float배열에 저장
        /// </summary>
        /// <param name="_currentTime">현재 GameManager의 점수 할당</param>
        public void SetBestTime(float _currentTime)
        {

            farrTimeRank = Convert<float>(CheckRank(_currentTime, iarrScoreRank, farrTimeRank), 3);
        }

        //현재점수 랭크 등록
        public void SetBestRecord(int _currentScore, float _currentTime)
        {
            SetBestScore(_currentScore);
            SetBestTime(_currentTime);
        }

        // 한번더 나눌수 있도록 해야함
        object[] CheckRank<T>(T data, int[] iarr, float[] farr)
        {
            int rankarrayidx = 3;
            object[] objarr = new object[3];

            if (data.GetType() == typeof(int))//현재 받은 데이터의 타입이 int형일때
            {
                for (int i = 0; i < 3; i++)
                {
                    if (iarr[i].CompareTo(data) < 0)
                    {
                        rankarrayidx = i;
                        break;
                    }//데이터 비교후 인덱스 할당
                    else
                    {
                        objarr[i] = iarr[i];
                    }// 기본 배열의 위치 유지
                }
                for (int i = 2; i >= rankarrayidx + 1; i--)
                {
                    objarr[i] = iarr[i - 1];
                }//배열의 남은 칸 할당
                if (rankarrayidx < 3)
                {
                    objarr[rankarrayidx] = data;
                }//비교한 데이터 할당
            }
            else//현재 받은 데이터의 타입이 float형일때
            {
                for (int i = 0; i < 3; i++)
                {
                    if (farr[i].CompareTo(data) > 0)
                    {
                        rankarrayidx = i;
                        break;
                    }
                    else { objarr[i] = farr[i]; }
                }
                for (int i = 2; i >= rankarrayidx + 1; i--)
                {
                    objarr[i] = farr[i - 1];
                }
                if (rankarrayidx < 3)
                {
                    objarr[rankarrayidx] = data;
                }
            }
            return objarr;
        }

        T[] Convert<T>(object[] a, int n)
        {
            T[] arr = new T[n];
            for (int i = 0; i < n; i++)
            {
                arr[i] = (T)a[i];

            }
            return arr;
        }

        #region Debug
        public void DebugRank()
        {
            if (iarrScoreRank != null)
            {
                for (int i = 0; i < 3; i++)
                {
                    Debug.Log((i + 1) + " : " + iarrScoreRank[i]);
                }
            }
            if (farrTimeRank != null)
            {
                for (int i = 0; i < 3; i++)
                {
                    Debug.Log((i + 1) + " : " + farrTimeRank[i]);
                }
            }

        }
        #endregion
        #region 미사용
        public void SetBestRecord(int n)
        {
            SetBestScore(n);

        }
        public void SetBestRecord(float f)
        {
            SetBestTime(f);
        }
        #endregion
    }
}
//static string SPLIT_RE = @",(?=(?:[^""]*""[^""]*"")*(?![^""]*""))"; csv파싱 위한 문자열 패턴의 정규 표현식