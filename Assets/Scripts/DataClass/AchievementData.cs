﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Custom {
    [System.Serializable]
    public class AchievementData
    {
        public static AchievementData INITDATA = new AchievementData(new AchievementContent[7]
            {new AchievementContent("1번 실수", 1),
            new AchievementContent("2번 실수", 2),
            new AchievementContent("3번 실수", 3),
            new AchievementContent("4번 실수", 4),
            new AchievementContent("5번 실수", 5),
            new AchievementContent("6번 실수", 6),
            new AchievementContent("7번 실수", 7)});

        Dictionary<string, AchievementContent> Datas = new Dictionary<string, AchievementContent>();
        List<string> Keys = new List<string>();

        public AchievementData(AchievementContent[] arrdata = null)
        {
            if (arrdata != null)
            {
                for (int i = 0; i < arrdata.Length; i++)
                {
                    Datas.Add(arrdata[i].STRING, arrdata[i]);
                    Keys.Add(arrdata[i].STRING);
                }
            }
        }//초기화
        public bool HaveAchievement(string key, ref string message)
        {
            if (Datas.ContainsKey(key))
            {
                if (!Datas[key].ISACHIEVE)
                {
                    return true;
                }
                else
                {
                    message = "Already achieve this data";
                    return false;
                }//이미 달성한 경우
            }
            else
            {
                message = "Please add this Achievement data";
                return false;
            }//key가 존재하지 않는 경우
        }
        //매개변수key를 받아 bool값 리턴, message를 통해 false이유 출력
        public int GetDataCount()
        {
            return Datas.Count;
        }
        //AchievementData전체 개수 반환
        public int GetKeyidx(string str)
        {
            return Keys.BinarySearch(str);
        }
        //Key인덱스 반환 함수
        public AchievementContent GetData(string key)
        {
            return Datas[key];
        }
        //AchievementContent호출 함수
        public AchievementContent GetData(int n)
        {
            return Datas[Keys[n]];
        }

        #region 미사용
        public void AddData(string Key, int condition, bool flg = false)
        {
            if (!Datas.ContainsKey(Key))
            {
                Datas.Add(Key, new AchievementContent(Key, condition, flg));
                Keys.Add(Key);
                Debug.Log("Data add");
            }
            else
            {
                Debug.Log("Data is already added");
            }
        }//AchievementData추가 함수
        #endregion
        #region Debug
        public void PrintAchieves()
        {
            for (int i = 0; i < Keys.Count; i++)
            {
                Debug.Log("Datas : " + Datas[Keys[i]].STRING + " : " + Datas[Keys[i]].ISACHIEVE);
            }
        }
        public void PrintStrings()
        {
            for (int i = 0; i < Keys.Count; i++)
            {
                Debug.Log("Datas : " + Datas[Keys[i]].STRING + " : " + Datas[Keys[i]].STRING);
            }
        }
        public void PrintConditions()
        {
            for (int i = 0; i < Keys.Count; i++)
            {
                Debug.Log("Datas : " + Datas[Keys[i]].STRING + " : " + Datas[Keys[i]].CONDITION);
            }
        }
        #endregion
    }
}