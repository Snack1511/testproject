﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockEffectScript : MonoBehaviour
{
    private Animator Anim;
    // Start is called before the first frame update
    void Start()
    {
        Anim = GetComponent<Animator>();
    }

    /// <summary>
    /// 애니메이션 재생위한 함수
    /// </summary>
    /// <param name="n">재생해야 할 블럭의 인덱스 번호 [붉은색 : 0, 푸른색 : 1]</param>
    public void PlayAnimation(int n)
    {
        Anim.Play("Block" + n.ToString() + "_Hit", 0);
    }
}
