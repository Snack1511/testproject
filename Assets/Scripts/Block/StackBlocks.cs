﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StackBlocks : MonoBehaviour
{
    int iTargetIdx;
    EffectControl Effect;
    Vector2 pos = Vector2.zero;
    Vector2 IncreaseValue = new Vector2(0, 0.5f);
    Vector2 InvisibleIncreaseValue;
    Vector3 InvisibleTopPos;
    GameObject Block;
    PlayManager Playmgr;
    List<BlockScript> lstBlocks = new List<BlockScript>();

    public int TAG
    {
        get{ return lstBlocks[iTargetIdx].TAG; }
    }
    public int SCORE
    {
        get { return lstBlocks[iTargetIdx].SCORE; }
    }

#region UnityEditor
    private void Awake()
    {
        InitBlockStack();
    }
    void Start()
    {
        ResetStackBlocks();
    }

    void Update()
    {
        try
        {
            Playmgr ??= GameManager.Instance.PLAYMGR ?? throw new System.ArgumentNullException(nameof(GameManager.Instance.PLAYMGR), "PlayMgr is Null");
            if (Playmgr.IsCorrectClick())
            {
                Playmgr.CalculateScore(SCORE);
                SortBlock();
                Playmgr.ReleaseClick();
            }
        }
        catch (System.ArgumentNullException)
        {
            Debug.Log("Hello");
        }

    }
#endregion

    /// <summary>
    /// 리셋이 필요할 경우 호출
    /// </summary>
    public void ResetStackBlocks()
    {
        for(int i = 0; i < lstBlocks.Count; i++)
        {
            lstBlocks[i].ResetBlockScript();
        }
    }


    //블럭 제거시 쌓는 함수
    void SortBlock()
    {
        SetPosition(iTargetIdx, InvisibleTopPos, false);//가장 아래 블럭 맨위로 이동
        SetVisible(iTargetIdx);
        Effect.PlayEffect(GetBlockTag(iTargetIdx));
        for (int i = 0; i < lstBlocks.Count; i++)
        {
            if (i != iTargetIdx)
            {
                SetPosition(i, -IncreaseValue);
            }
        }//나머지 블럭 아래로 이동

        iTargetIdx = (iTargetIdx + 1)% lstBlocks.Count;//타겟 인덱스 재설정
        for(int i = 0; i < 8; i++)
        {
            lstBlocks[CalculateBlockidx(i+iTargetIdx, lstBlocks.Count)].SetOrderIndex(i);
        }
    }

    //블럭 오브젝트들 전체 생성 후 초기화
    void InitBlockStack()
    {
        InvisibleIncreaseValue = (IncreaseValue * 4);
        Block = Resources.Load<GameObject>("Prefabs/Block");
        for (int i = 0; i < 15; i++)
        {
            var obj = Instantiate(Block).gameObject;
            obj.transform.SetParent(gameObject.transform);
            obj.transform.position = pos;

            if (i == 7)
            {
                pos += InvisibleIncreaseValue;
            }
            pos += IncreaseValue;
            if (i > 7)
            {
                obj.SetActive(false);
            }

            lstBlocks.Add(obj.GetComponent<BlockScript>());
        }
        for (int i = 0; i < 15; i++)
        {
            lstBlocks[i].GetTargetPosition(lstBlocks[CalculateBlockidx(i - 1, 15)].TR, IncreaseValue);
            if (i < 8)
            {
                lstBlocks[i].SetOrderIndex(i);
            }
        }
        InvisibleTopPos = lstBlocks[lstBlocks.Count - 1].transform.position;
        Effect = GameObject.Find("Effects").GetComponent<EffectControl>();
        iTargetIdx = 0;
    }

    //블럭 활성화
    void SetVisible(int n, bool flg = false)
    {
        int visibleBlockidx = (n + 8) % lstBlocks.Count;
        lstBlocks[n].gameObject.SetActive(flg);
        lstBlocks[visibleBlockidx].gameObject.SetActive(!flg);
    }

    //블럭 위치 재설정
    void SetPosition(int _idx, Vector2 _settingposition, bool _operateflg = true)
    {
        lstBlocks[_idx].ResetPosition(_settingposition, _operateflg);
    }

    /// <summary>
    /// 블럭의 idx에 제한을 거는 함수
    /// </summary>
    /// <param name="_idx">현재 블럭의 idx값</param>
    /// <param name="_totalcount"> 제한 할 수 </param>
    /// <returns> (_idx + _totalcount) % _totalcount </returns>
    int CalculateBlockidx(int _idx, int _totalcount)
    {
        return (_idx + _totalcount) % _totalcount;
    }

    //블럭 종류 태그 호출함수
    int GetBlockTag(int _idx)
    {
        return lstBlocks[_idx].GetComponent<BlockScript>().TAG;
    }
    
}
