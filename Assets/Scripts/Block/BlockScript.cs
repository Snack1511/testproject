﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using Custom;


public class BlockScript : MonoBehaviour
{

    private int Tag;//현재 블럭의 종류
    private float fMovespd = 5f;//블럭 하강 속도

    private GameObject Block0, Block1, Block2;//자식오브젝트로 할당된 블럭프리팹
    private Transform targetPos;//타켓 오브젝트의 위치
    private Vector2 IncreaseValue;//타겟 오브젝트설정 이후 추가값
    private SortingGroup Sgroup;//렌더링 순서
    private BlockData Data;//블럭정보

    public int TAG {  get { return Data.TAG; } }
    public int SCORE { get { return Data.SCORE; } }
    public Transform TR { get { return gameObject.transform; } }

#region UnityMethod
    private void Awake()
    {
        Data = new BlockData();
        Block0 = transform.GetChild(0).gameObject;
        Block1 = transform.GetChild(1).gameObject;
        Block2 = transform.GetChild(2).gameObject;
        Sgroup = GetComponent<SortingGroup>();
    }//블럭상태 초기화
    private void OnEnable()
    {
        ResetBlockScript();//블럭 재설정
    }//블럭 오브젝트 활성화 시 마다 동작
    void Update()
    {
        MoveBlock(IsArriveGoal());
    }//블럭의 움직임 구현
    #endregion

    /// <summary> 블럭 재설정 </summary>
    public void ResetBlockScript()
    {
        ResetBlock();
        ChangeBlock(Data.TAG);
    }

    /// <summary>
    /// 블럭 비활성화된 후의 위치 재설정
    /// </summary>
    /// <param name="_increasevalue">연산될 벡터값</param>
    /// <param name="Increaseflg">연산 형태 [true : 증가] [false : 대입]</param>
    public void ResetPosition(Vector2 _increasevalue, bool Increaseflg)
    {
        if (Increaseflg)
        {
            transform.position += (Vector3)_increasevalue;
        }
        else
        {
            transform.position = _increasevalue;
        }
    }

    /// <summary>
    /// 현재 블럭의 아래에 위치하는 블럭의 위치값 가져오기
    /// </summary>
    public void GetTargetPosition(Transform _targetpos, Vector2 _increasevalue)
    {
        targetPos = _targetpos;
        IncreaseValue = _increasevalue;
    }

    /// <param name="_index">낮을 수록 먼저 랜더링</param>
    public void SetOrderIndex(int _index)
    {
        Sgroup.sortingOrder = _index;
    }

    /// <summary>
    /// 블럭 랜덤 초기화
    /// </summary>
    /// <param name="_index">[min, max)</param>
    void ResetBlock(int min = 1, int max = 3)
    {
        int index = Random.Range(min, max);
        Data.ResetData(index, false);
        Tag = Data.TAG;
    }

    /// <summary>
    /// 태그에 따라 활성화 시킬 블럭 프리팹 선택
    /// </summary>
    void ChangeBlock(int _index)
    {
        if(_index == 1)
        {
            Block0.SetActive(true);
            Block1.SetActive(false);
            Block2.SetActive(false);
        }
        else if (_index == 2)
        {

            Block0.SetActive(false);
            Block1.SetActive(true);
            Block2.SetActive(false);
            
        }
        else
        {
            Block0.SetActive(false);
            Block1.SetActive(false);
            Block2.SetActive(true);
        }
    }

    /// <summary> 
    /// <para/>블럭의 도착상태에 따라 블럭 움직임 변화
    /// <para/>도착상태는 <see cref="IsArriveGoal"/>를 참조
    /// </summary> 
    /// <param name="_moveflg">[목표 이전 : 1] [목표 초과 : 2]</param>
    void MoveBlock(int _moveflg)
    {
        if (_moveflg == 1)
        {
            gameObject.transform.Translate(Vector3.down * fMovespd * Time.deltaTime);//도달 전 일 경우
        }
        else if(_moveflg == 2)
        {
            ResetPosition(targetPos.position + (Vector3)IncreaseValue, false);//목표점을 넘은 경우
        }
    }
    /// <summary>
    /// 아래블럭의 위치를 바탕으로 블럭 움직임의 도착점 설정 및 도착 후의 상태 설정 
    /// <para/>[1 : 도착전]
    /// <para/>[2 : 도착후 오버]
    /// <para/>[0 : 목표와 같은 위치]
    /// <para/>[-1 : 목표위치 설정X]
    /// </summary>
    int IsArriveGoal()
    {
        if (targetPos != null && targetPos.gameObject.activeSelf)
        {
            if (targetPos.position.y + IncreaseValue.y < transform.position.y)
            {
                return 1;//도착전
            }
            else if (targetPos.position.y + IncreaseValue.y > transform.position.y)
            {
                return 2;//도착 후 오버
            }
            else
            {
                return 0;//목표점과 같은 위치
            }
        }
        else
        {
            return -1;//목표로 한 위치가 설정되지 않은 경우
        }
    }
}
