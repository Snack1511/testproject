﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectControl : MonoBehaviour
{
    [SerializeField] private GameObject Effect;
    private int iPlayidx;
    private List<BlockEffectScript> lstEffects = new List<BlockEffectScript>();

    // Start is called before the first frame update
    void Start()
    {
        iPlayidx = 0;
        for (int i = 0; i < 8; i++)
        {
            AddEffect(i);
        }
    }

    /// <summary>
    /// 애니메이션 재생함수
    /// </summary>
    /// <param name="_blockidx">재생할 블럭의 종류</param>
    public void PlayEffect(int _blockidx)
    {
        lstEffects[iPlayidx].PlayAnimation(_blockidx);
        iPlayidx = (iPlayidx + 1)%8;
    }

    //List추가 함수
    private void AddEffect(int _idx)
    {
        lstEffects.Add(Instantiate(Effect).GetComponent<BlockEffectScript>());
        lstEffects[_idx].transform.SetParent(gameObject.transform);
    }
}
