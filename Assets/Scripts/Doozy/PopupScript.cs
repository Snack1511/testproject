﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Doozy.Engine.UI;

public class PopupScript : MonoBehaviour
{
    /// <summary>
    /// 팝업생성함수
    /// </summary>
    /// <param name="_popupname">팝업이름</param>
    public void ShowPopup(string _popupname)
    {
        UIPopupManager.ShowPopup(_popupname, false, true);
    }

    /// <summary>
    /// 팝업제거함수
    /// </summary>
    /// <param name="_popupname">팝업이름</param>
    public void HIdePopup(string _popupname)
    {
        UIPopup.HidePopup(_popupname);
    }

    /// <summary>
    /// NoticePopup 정보할당함수
    /// </summary>
    /// <param name="_noticetag">정보태그</param>
    public void SetNotice(string _noticetag)
    {
        NoticeScript.NoticeTag = _noticetag;
    }

    /// <summary>
    /// RankPopup 정보할당함수
    /// </summary>
    /// <param name="_ranktype">랭크타입</param>
    public void SetRankType(string _ranktype)
    {
        RankboardScript.strRankType = _ranktype;
    }
}
