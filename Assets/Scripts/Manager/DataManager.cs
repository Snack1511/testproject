﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Custom;
public class DataManager : MonoBehaviour
{
    public string SavePath;

    RankData BestRecord;
    public RankData BESTRECORD
    {
        get { return BestRecord; }
    }
    #region UnityMethod
    private void Awake()
    {
#if UNITY_EDITOR
        SavePath = Application.dataPath + "/SaveData";
        Debug.Log(SavePath);
#else
        SavePath = Application.persistentDataPath + "/SaveData";
#endif
        CreateFolder(SavePath);
        BestRecord = DataSave.Load<RankData>(SavePath);
        if (BestRecord == null)
        {
            OverwriteData(0, 999);
        }
        GameManager.Instance.DataSaveEvent.AddListener(OverwriteData);
        //GameManager.Instance.GameResetEvent.AddListener()
    }

    // Start is called before the first frame update
    void Start()
    {
        GameManager.Instance.UpdateBestRecord();
    }

    #endregion
    /// <summary>
    /// 매니저 초기화 용
    /// </summary>
    /// <param Data ="data"> 데이타매니저를 초기화 할 시 넣어야 할 Data클래스 변수</param>
    public void ResetDataMgr(RankData data)
    {
        BestRecord = data;
    }

    /// <summary>
    /// 변수에 데이터 저장
    /// </summary>
    /// <param name="n">점수</param>
    /// <param name="f">시간</param>
    public void OverwriteData(int n, float f)
    {
        if (SavePath != null)
        {
            if (BestRecord == null)
            {
                BestRecord = new RankData(n, f);
                
            }
            else
            {
                BestRecord.SetBestRecord(n, f);

            }
            DataSave.Save(SavePath, BestRecord);
        }
    }

    //삭제버튼 입력시 호출
    public void DeleteData()
    {
        DataSave.Delete(SavePath);
        ResetData();
        BestRecord = DataSave.Load<RankData>(SavePath);
        GameManager.Instance.RECORD = BestRecord;
    }

    #region 애매한 부분
    public void SaveAchievement(AchievementData data)
    {
        DataSave.Save<AchievementData>(SavePath, data, "/Achieve.sv");
#if UNITY_EDITOR
        Debug.Log("SaveAchieve");
#endif
    }

    
    public AchievementData LoadAchievement()
    {
        AchievementData Achievements = DataSave.Load<AchievementData>(SavePath, "/Achieve.sv");
        if (Achievements == null)
        {
#if UNITY_EDITOR
            Debug.Log("SaveNewAchieve");
#endif
            Achievements = AchievementData.INITDATA;
            SaveAchievement(Achievements);
        }
        return Achievements;
    }
#endregion

    void ResetData()
    {
        BestRecord = new RankData(0, 999);
        DataSave.Save(SavePath, BestRecord);
    }

    void CreateFolder(string path)
    {
        DirectoryInfo Dinfo = new DirectoryInfo(path);
        if (!Dinfo.Exists)
        {
#if UNITY_EDITOR
            Debug.Log("CreateFolder");
#endif
            Dinfo.Create();
        }
    }
}
