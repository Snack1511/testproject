﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PlayManager : MonoBehaviour
{
    public bool bGameEnd;
    public static PlayManager Instance;
    [Header(" - 스코어 표시")]
    [SerializeField] private GameObject RecordImgs;
    [SerializeField] private Text RecordText;
    [Space(2)]
    [Header(" - 타이머 및 골 슬라이더")]
    [SerializeField] private Slider TimerSlider;
    [SerializeField] private GameObject GoalProgressObj;//필요한가..?
    [SerializeField] private Slider GoalSlider;
    [Space(2)]
    [Header(" - 스크립트 파일들")]
    [SerializeField] private AchievementManager AchievementMgr;
    [SerializeField] private PlayerScript Player;
    [SerializeField] private StackBlocks Blocks;
    [SerializeField] private Custom.ModeData Mode;

    private bool bCorrect;
    private bool bPlayStart;
    private bool bPause;
    private bool bPlayEnd;
    private int imissCnt;
    private int iGoal;
    private float fRemainTime;
    private float fTotalRecord;
    private float fBaseTime;
    private float fComboTime;
    private Color sliderBgCol = new Color(0.25f, 0.48f, 0, 1f);
    private Color sliderFillCol = new Color(0.5f, 1, 0.2f, 1f);
    private Color ResetBgColor = new Color(0.25f, 0.48f, 0, 1f);
    private Color ResetFillColor = new Color(0.5f, 1, 0.2f, 1f);

    public bool CORRECT
    {
        get { return bCorrect; }
    }
    public bool PLAYSTART
    {
        get { return bPlayStart; }
    }

    #region UnityMethod
    private void Awake()
    {
        Init();

    }

    void Update()
    {
        if (!bPause)
        {
            if (bPlayStart)
            {
                if (Time.timeScale == 0)
                {
#if UNITY_EDITOR
                    Debug.Log("TimeScale 1");
#endif
                    Time.timeScale = 1;
                }
                if (!bPlayEnd)
                {
                    SetTimeSliderValue();
                }
                IsCorrectBlock();
                IsPlayEnd();
            }
            
        }
        else if (Time.timeScale == 1)
        {
            Time.timeScale = 0;
        }
        
    }
    #endregion

    public void ResetPlayMgr()
    {
        ResetRecords();
        TimerSlider.transform.GetChild(0).GetComponent<Image>().color = ResetBgColor;
        TimerSlider.transform.GetChild(1).GetChild(0).GetComponent<Image>().color = ResetFillColor;
        sliderBgCol = TimerSlider.transform.GetChild(0).GetComponent<Image>().color;
        sliderFillCol = TimerSlider.transform.GetChild(1).GetChild(0).GetComponent<Image>().color;
        
        bPause = false;
        bPlayStart = false;
        bPlayEnd = false;
        bCorrect = false;
        Mode = null;
        Blocks.ResetStackBlocks();
    }

    /// <summary>
    /// <see cref="GameManager.ModeSetEvent"/>에서 호출
    /// </summary>
    /// <param name="Data"></param>
    public void SetPlay(Custom.ModeData Data)
    {
        Mode = Data;
        bPlayStart = true;
        SelectMode();
        ResetRecords();
    }
    
    
    public bool IsCorrectClick()
    {
        if (bPlayStart)
        {
            if (Player.CLICK)
            {
                if (bCorrect)
                {
                    iGoal++;
                    SetProgressSliderValue((float)iGoal);
                    ReleaseClick();
                    if (Mode.GOAL > 0)
                    {
                        WorkCombo();
                    }
#if UNITY_EDITOR
                    Debug.Log("Right Click");
#endif
                    return true;
                }
                else
                {
                    imissCnt++;
                    AchievementMgr.AchieveCountingCondition("번 실수", imissCnt);
                    ReleaseClick();
                    CalculateComboTime();
#if UNITY_EDITOR
                    Debug.Log("Wrong Click");
#endif
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public void ReleaseClick()
    {
        //Player->PlayMgr->Stackblock 단계로 호출
        Player.ReleaseClick();
    }

    public void CalculateScore(int n)
    {
        if (Mode.GOAL <= 0)
        {
            fTotalRecord += n;
            SetRecordText(fTotalRecord);
        }
    }
    /// <summary>
    /// <see cref="GameManager.GamePauseEvent"/>에서 호출
    /// </summary>
    /// <param name="flg"></param>
    public void IsPauseGame(bool flg = false)
    {
        bPause = flg;
    }
    /// <summary>
    /// <see cref="GameManager.GameResetEvent"/>에서 호출
    /// </summary>
    public void ResetRecords()
    {
        imissCnt = 0;
        iGoal = 0;
        fRemainTime = 0;
        fTotalRecord = 0;
        fBaseTime = Time.time;
        fComboTime = -100f;

        SetRecordText(fTotalRecord);
        CalculateTimeSlider(0);
        SetProgressSliderValue(iGoal);
    }

    void Init()
    {
        GameManager gameMgr = GameManager.Instance;
        gameMgr.ModeSetEvent.AddListener(SetPlay);
        gameMgr.GamePauseEvent.AddListener(IsPauseGame);
        gameMgr.GameResetEvent.AddListener(ResetPlayMgr);
        gameMgr.GameRestartEvent.AddListener(ResetPlayMgr);
        //ResetPlayMgr();
    }
    //이전에 플래이어가 선택한 블럭과 현재 가장 아래에 위치한 블럭의 태그값 비교
    void IsCorrectBlock()
    {
        if (Player.TAG == Blocks.TAG)
        {

            bCorrect = true;
        }
        else
        {
            bCorrect = false;
        }
    }

    //조건들중 하나라도 달성되면 종료신호
    void IsPlayEnd()
    {
        if (!bPlayEnd)
        {
            if ((imissCnt >= Mode.MISS || fRemainTime >= Mode.TIME) && iGoal >= Mode.GOAL)
            {
                SendRecord();
                bPlayEnd = true;
                GameManager.Instance.GameEndEvent.Invoke();
                //플레이 종료
                //GameEventMessage.SendEvent("GameEnd");
            }
        }
    }
    #region 점수 UI
    //선택된 모드따라 호출할 점수 UI변경
    void SelectMode()
    {
        if (RecordImgs)
        {
            if (Mode == Custom.ModeData.STONES100)
            {
                TimerSlider.maxValue = 5f;
                RecordImgs.transform.GetChild(0).gameObject.SetActive(false);
                RecordText.gameObject.SetActive(false);
                GoalProgressObj.SetActive(true);
            }
            else
            {
                TimerSlider.maxValue = 100f;
                RecordImgs.transform.GetChild(0).gameObject.SetActive(true);
                RecordText.gameObject.SetActive(true);
                GoalProgressObj.SetActive(false);
            }
        }
#if UNITY_EDITOR
        else
        {
            Debug.Log("RecordImgs is null");
        }
#endif
    }
    
    //점수UI로 현재 점수 전송
    void SendRecord()
    {
        if (Mode.GOAL > 0)
        {
            fTotalRecord = fRemainTime;
        }
        GameManager.Instance.SetRecord(fTotalRecord);
    }
    //점수UI의 값 변경
    void SetRecordText(float f, bool flg = false)
    {
        if (RecordText != null)
        {
            RecordText.text = flg ? f.ToString("N2") : f.ToString();
        }
    }
    #endregion

    #region 콤보

    //콤보루틴
    void WorkCombo()
    {
        if (fComboTime - Time.time > -5f)
        {
#if UNITY_EDITOR
            Debug.Log("Combo적용");
#endif
            fBaseTime += ((fComboTime - Time.time + 5f) * 0.05f);
            CheckComboStart();
        }
        else
        {
#if UNITY_EDITOR
            Debug.Log("Combo초기화");
#endif
            CheckComboStart();
        }
    }

    //콤보 초기화
    void CheckComboStart()
    {
        fComboTime = Time.time;
    }

    //콤보시간계산
    void CalculateComboTime()
    {

        if (Mode.GOAL > 0)
        {
            fComboTime = (Time.time > fComboTime + 5f) ? Time.time - 5f : fComboTime - 1;
        }
    }
    #endregion

    #region 슬라이더
    void SetProgressSliderValue(float value)
    {
        if (Mode != null)
        {
            if (Mode.GOAL > 0)
            {
                GoalSlider.value = value;
            }
        }
    }
    void SetTimeSliderValue()
    {
        fRemainTime = Time.time - fBaseTime;
        if (Mode.TIME > 0)
        {
            CalculateTimeSlider(Mode.TIME - fRemainTime);
        }
        if (Mode.GOAL > 0)
        {
            CalculateTimeSlider(5f + (fComboTime - Time.time));
        }

    }
    void CalculateTimeSlider(float _value)
    {
        TimerSlider.value = _value;
        if (_value != 0)
        {
            TimerSlider.transform.GetChild(0).GetComponent<Image>().color = CalColor(_value, TimerSlider, sliderBgCol, 0.25f, 0.48f);
            TimerSlider.transform.GetChild(1).GetChild(0).GetComponent<Image>().color = CalColor(_value, TimerSlider, sliderFillCol, 0.5f, 1);
        }
        else
        {
            TimerSlider.transform.GetChild(0).GetComponent<Image>().color = ResetBgColor;
            TimerSlider.transform.GetChild(1).GetChild(0).GetComponent<Image>().color = ResetFillColor;
        }
    }
    Color CalColor(float f, Slider slider,Color col, float redincrease, float greenincrease)
    {
        col.r = (2f - (f / slider.maxValue)) * redincrease;//0.25 -> 0.5, 0.5 -> 1
        col.g = (f / slider.maxValue) * greenincrease;//0.48 -> 0, 1 -> 0
        return col;
    }
    #endregion
}
//55 123 54