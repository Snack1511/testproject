﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Custom;
using Doozy.Engine.UI;
using UnityEngine.Events;
public class AchievementManager : MonoBehaviour
{
    
    public static AchievementManager instance;
    public UnityEvent<int> AcheiveEvent;

    [SerializeField] private DataManager DataMgr;
    private AchievementData AchieveDatas;

    public AchievementData DATA
    {
        get { return AchieveDatas; }
    }
    // Start is called before the first frame update
    private void Awake()
    {
        AchieveDatas = DataMgr.LoadAchievement();
        if (instance != null && GameObject.Find("AchievementMgr"))
        {
            Destroy(gameObject);
        }
        if(instance == null)
        {
            instance = this;
        }

    }
    //조건 달성시 호출
    public void AchieveCondition(string key, int condition) {
        string message = "";
        if (AchieveDatas.HaveAchievement(key, ref message))
        {
            AchieveDatas.GetData(key).SetAchieve(condition, true);
            AcheiveEvent.Invoke(AchieveDatas.GetKeyidx(key));
            DataMgr.SaveAchievement(AchieveDatas);
            OnAchievePopup("달성", key);
        }
        else
        {
#if UNITY_EDITOR
            Debug.Log(message);
#endif
        }
    }
    //조건 달성시 호출
    public void AchieveCountingCondition(string key, int condition)
    {
        key = condition.ToString() + key;
        AchieveCondition(key, condition);
    }

    void OnAchievePopup(string title="제목", string msg="내요오오오오옹")
    {
        UIPopupManager.ShowPopup("Popup HitStone-Achieve", false, false).
            GetComponent<AchievemetPopupScript>().PrintAchieveText(title, msg);
    }
}
