﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Doozy.Engine.Events;
using Doozy.Engine;
using Doozy.Engine.Soundy;
public class ResultManager : MonoBehaviour
{
    [SerializeField] private Text MyRecord;
    [SerializeField] private GameObject BestTitles;
    [SerializeField] private GameObject Titles;
    [SerializeField] private GameObject RankContainer;
    private Text[] Ranks = new Text[3];
    SoundyController GameEndSound;

    private void Awake()
    {
        MyRecord = GameObject.Find("txtNowRecord").GetComponent<Text>();
        BestTitles = GameObject.Find("ImgBestTitles");
        Titles = GameObject.Find("ImgTitles");
        //GameObject obj = GameObject.Find("txtRanks");
        for (int i = 0; i < RankContainer.transform.childCount; i++)
        {
            Ranks[i] = RankContainer.transform.GetChild(i).GetComponent<Text>();
        }
        GameManager.Instance.GameEndEvent.AddListener(SetResult);
    }

    //GameManager에서 호출..?
    public void UpdateRank(Custom.ModeData Mode, Custom.RankData data, int score, float time)
    {
        if (Mode != null)
        {
            if (Mode == Custom.ModeData.STONES100)
            {

                PrintRank(data.TIMERANK, time);
            }
            else
            {

                PrintRank(data.SCORERANK, score);
            }
        }
    }

    //결과 출력
    // GameEventListener에서 사용
    public void SetResult()
    {
        if (GameEndSound != null)
        {
            GameEndSound = GameManager.Instance.PlaySoundy("GameUISound", "GameEnd");
        }
        PrintTitles(GameManager.Instance.MODE);
        if (GameManager.Instance.MODE == Custom.ModeData.STONES100)
        {
            PrintRecord(GameManager.Instance.MYTIME);
        }
        else
        {
            PrintRecord(GameManager.Instance.MYSCORE);
        }
    }

    //순위 출력
    void PrintRank<T>(T[] arr, T data)
    {
        bool bDataCheck = false;
        int idx = 0;
        for (int i = 0; i < Ranks.Length; i++) {
            if (!bDataCheck)
            {
                if (Custom.RankData.CheckData(data, arr[i]))
                {
                    Ranks[i].text = GetString(data);
                    bDataCheck = true;
                }
                else
                {
                    Ranks[i].text =GetString(arr[idx]);
                    idx++;
                }
            }
            else
            {
                Ranks[i].text = GetString(arr[idx]);
                idx++;
            }
        }
    }

    //점수 출력
    void PrintRecord<T>(T _value)
    {
        MyRecord.text = GetString(_value);
    }

    //데이터의 타입에따라 출력 방법 변경
    string GetString<T>(T data)
    {
        if(data.GetType() == typeof(float))
        {
            float fData = (float)(object)data;
            return fData.ToString("N2");
        }
        else
        {
            return data.ToString();
        }
    }
    

    void PrintTitles(Custom.ModeData mode)
    {
        OnBestTitles((mode == Custom.ModeData.STONES100));
        OnTitles((mode == Custom.ModeData.STONES100));
        
    }
    void OnBestTitles(bool IsStone100)
    {
        BestTitles.transform.GetChild(0).gameObject.SetActive(!IsStone100);
        BestTitles.transform.GetChild(1).gameObject.SetActive(IsStone100);
    }
    void OnTitles(bool IsStone100)
    {
        Titles.transform.GetChild(0).gameObject.SetActive(!IsStone100);
        Titles.transform.GetChild(1).gameObject.SetActive(IsStone100);
    }
    
    

}
