﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Custom;
using Doozy.Engine.Soundy;
using Doozy.Engine;
using UnityEngine.Events;
public class GameManager : MonoBehaviour
{
    
    public static GameManager Instance;

    [HideInInspector] public UnityEvent<ModeData> ModeSetEvent;
    [HideInInspector] public UnityEvent GameEndEvent;
    [HideInInspector] public UnityEvent GameResetEvent;
    [HideInInspector] public UnityEvent GameStartEvent;
    [HideInInspector] public UnityEvent<int, float> DataSaveEvent;
    [HideInInspector] public UnityEvent DataLoadEvent;
    [HideInInspector] public UnityEvent<bool> GamePauseEvent;
    [HideInInspector] public UnityEvent GameRestartEvent;

    [Space(2)]
    [Header(" - Managers")]
    [SerializeField] ResultManager ResultMgr;
    [SerializeField] DataManager DataMgr;
    [SerializeField] PlayManager PlayMgr;
    [Space(2)]
    [Header(" - For Debug")]
    [SerializeField] RankData Record = new RankData(0, 999);
    [SerializeField] ModeData Mode;

    bool bGameStart;

    int iNowScore = 0;
    float fNowTime = 999;

    SoundyController BGMContorller;

    public int MYSCORE
    {
        get { return iNowScore; }
    }
    public float MYTIME
    {
        get { return fNowTime; }
    }
    public ModeData MODE
    {
        get {
#if UNITY_EDITOR
            if (Mode != null)
            {
                Mode.DebugModeData();
            }
            else
            {
                Debug.Log("Mode is null");
            }
#endif
            return Mode;
        }
    }
    public RankData RECORD
    {
        get { return Record; }
        set { Record = value; }
    }
    public PlayManager PLAYMGR
    {
        get { return PlayMgr; }
    }
    //public ModeManager MODEMGR {
    //    get { return ModeMgr; }
    //}

    #region UnityMethod
    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
        }
        if (Instance == null)
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;
        }
        InitGameMgr();
    }
    void Start()
    {
        GameEndEvent.AddListener(ShoutGameEnd);
        GameResetEvent.AddListener(ResetGameMgr);
        DataLoadEvent.AddListener(UpdateBestRecord);
        PlayBGM();
    }
    void Update()
    {
        if (Mode != null && !bGameStart)
        {
            StopBGM();
            ModeSetEvent.Invoke(Mode);
            GameEventMessage.SendEvent("GameStart");//Nody에서 수신
            StartCoroutine("WaitTime", 1);
            bGameStart = true;
        }
    }
    #endregion

    public void SetMode(ModeData d)
    {
        Mode = d;
    }
    #region Record
    public void SetRecord(float n)
    {
        if (Mode.GOAL < 1)
        {
            iNowScore = (int)n;
        }
        else
        {
            fNowTime = n;
        }
    }
    public int[] GetScoreRank()
    {
        if (Record != null)
        {
#if UNITY_EDITOR
            Record.DebugRank();
#endif
            return Record.SCORERANK;
        }
        else
        {
            return null;
        }
    }
    public float[] GetTimeRank()
    {
        return Record.TIMERANK;
    }
    //최고기록 갱신함수
    public void UpdateBestRecord()
    {
        if (DataMgr != null)
        {
            Record = DataMgr.BESTRECORD;
        }
        if (ResultMgr != null && Record != null && Mode != null)
        {
            ResultMgr.UpdateRank(Mode, Record, iNowScore, fNowTime);
        }
    }
    #endregion
    
    #region Call Event Listener
    public void CallGamePause(bool flg)
    {
        GamePauseEvent.Invoke(flg);
    }
    public void CallRestart()
    {
        GameRestartEvent.Invoke();
    }
    public void CallReset()
    {
        GameResetEvent.Invoke();
    }
    #endregion

    #region Soundy
    public void PlayBGM()
    {
        if (BGMContorller == null)
        {
            BGMContorller = SoundyManager.Play("GameMusic", "BackgroundMusic");
        }
        else
        {
            BGMContorller.Play();
        }
    }
    public void StopBGM()
    {
        if (BGMContorller != null)
        {
            BGMContorller.Kill();
        }
    }
    public SoundyController PlaySoundy(string databaseName, string soundName)
    {
        return SoundyManager.Play(databaseName, soundName);

    }
    #endregion

    //초기화
    private void InitGameMgr()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        DataMgr = GetManager(DataMgr, GameObject.Find("DataMgr"));
        PlayMgr = GetManager(PlayMgr, GameObject.Find("PlayMgr"));
        ResultMgr = GetManager(ResultMgr, GameObject.Find("ResultMgr"));
        Mode = null;
    }
    private void ResetGameMgr()
    {
        PlayBGM();
        Mode = null;
        bGameStart = false;
    }
    //매니저 반환 함수
    private T GetManager<T>(T Manager, GameObject Obj)
    {
        if (Manager == null && Obj != null)
        {
            Manager = Obj.GetComponent<T>();
        }
        return Manager;
    }
    private void ShoutGameEnd()
    {
        GameEventMessage.SendEvent("GameEnd");
        DataLoadEvent.Invoke();
        DataSaveEvent.Invoke(iNowScore, fNowTime);
    }
    IEnumerator WaitTime(float _time)
    {
        yield return new WaitForSeconds(_time);
        GameStartEvent.Invoke();
    }
    

}
