﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Custom;
public class DataSave : MonoBehaviour
{
    /// <summary>
    /// 데이터 저장
    /// </summary>
    /// <typeparam name="T"> 저장할 타입</typeparam>
    /// <param name="path"> 
    /// <para/>저장경로 
    /// <para/>Application.persistentDataPath // c:\Users\사용자이름\AppData\LocalLow\회사이름
    /// <para/>Application.streamingAssetsPaht // 해당 프로젝트폴더 경로/Assets
    /// <para/>Application.dataPaht // 해당 프로젝트 폴더
    /// </param>
    /// <param name="data">저장할 데이터</param>
    /// <param name="name">저장할 데이터의 파일명</param>
    public static void Save<T>(string path, T data, string name = "/Save.sv")
    {
        path += name;
        BinaryFormatter format = new BinaryFormatter();//BinaryFormatter 객체 생성
        
        FileStream fstream = new FileStream(path, FileMode.Create);//FileStream 객체 생성

        format.Serialize(fstream, data); //Formatter객체의 Serialize함수 사용
        fstream.Close();//FileStream 닫음
    }

    /// <summary>
    /// 데이터 읽기
    /// </summary>
    /// <typeparam name="T"> 읽을 타입</typeparam>
    /// <param name="path"> 
    /// <para/>불러올 경로 
    /// <para/>Application.persistentDataPath // c:\Users\사용자이름\AppData\LocalLow\회사이름
    /// <para/>Application.streamingAssetsPaht // 해당 프로젝트폴더 경로/Assets
    /// <para/>Application.dataPaht // 해당 프로젝트 폴더
    /// </param>
    /// <param name="name">읽을 데이터의 파일명</param>
    public static T Load<T>(string path, string name = "/Save.sv") where T:class
    {
        path += name;
        if (File.Exists(path))
        {
            BinaryFormatter format = new BinaryFormatter();
            FileStream fstream = new FileStream(path, FileMode.Open);
            T data = format.Deserialize(fstream) as T;
            fstream.Close();
            return data;
        }
        else
        {
            Debug.Log("Not exist file");
            return null;
        }
    }

    /// <summary>
    /// 저장 파일 삭제
    /// </summary>
    /// <param name="path">
    /// <para/>저장 경로 
    /// <para/>Application.persistentDataPath // c:\Users\사용자이름\AppData\LocalLow\회사이름
    /// <para/>Application.streamingAssetsPaht // 해당 프로젝트폴더 경로/Assets
    /// <para/>Application.dataPaht // 해당 프로젝트 폴더
    /// </param>
    /// <param name="name">데이터 파일 명</param>
    /// <returns></returns>
    public static bool Delete(string path, string name= "/Save.sv")
    {
        path += name;
        if (File.Exists(path))
        {
            File.Delete(path);
            return true;
        }
        else
        {
            Debug.Log("Not exist file");
            return false;
        }

    }
    
}
/*파일 입출력 : Sustem.IO, Systme.Runtime.Serialization.Formatters.Binary
 *  참고 : https://xlfksh48.tistory.com/10, https://docs.microsoft.com/ko-kr/dotnet/api/system.io.file.exists?view=net-5.0
 * Application.persistentDataPath // c:\Users\사용자이름\AppData\LocalLow\회사이름
 * Application.streamingAssetsPaht // 해당 프로젝트폴더 경로/Assets
 * Application.dataPaht // 해당 프로젝트 폴더
 * 
 */
