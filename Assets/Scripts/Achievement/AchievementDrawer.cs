﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class AchievementDrawer : MonoBehaviour
{
    [SerializeField] GameObject content;
    [SerializeField] GameObject text;
    [SerializeField] ScrollRect scroll;
    [SerializeField] Font fontData;
    [SerializeField] Sprite OverlayImg;
    List<Text> texts = new List<Text>();
    // Start is called before the first frame update
    #region UnityMethod
    void Start()
    {
        InitAchievement(AchievementManager.instance.DATA.GetDataCount());
        AchievementManager.instance.AcheiveEvent.AddListener(UpdateAchievementDrawer);
    }
    #endregion

    //AchievementManager에서 호출
    public void UpdateAchievementDrawer(int n)
    {
        texts[n].color = Color.red;
        texts[n].transform.parent.GetComponent<Image>().color = new Color(0, 0, 0, 0.25f);
    }

    //도전과제 창 초기화
    void InitAchievement(int n = 20) {
        Image Overlay;
        Text txt;
        for (int i = 0; i < n; i++)
        {
            #region 오버레이설정
            Overlay = new GameObject("TextOverlay").AddComponent<RectTransform>().gameObject.AddComponent<Image>();
            Overlay.gameObject.AddComponent<AspectRatioFitter>().aspectMode = AspectRatioFitter.AspectMode.WidthControlsHeight;
            Overlay.GetComponent<AspectRatioFitter>().aspectRatio = 5.0f;
            Overlay = SetOverlay(Overlay);
            Overlay.transform.SetParent(content.transform);
            #endregion

            #region 텍스트객체 설정
            txt = new GameObject("Text").AddComponent<RectTransform>().gameObject.AddComponent<Text>();
            txt.transform.SetParent(Overlay.transform);
            texts.Add(txt.GetComponent<Text>());
            texts[i] = SetText(texts[i]);
            texts[i].text = AchievementManager.instance.DATA.GetData(i).STRING;//Achievement메니저에서 데이터들을 불러오는 부분
            if (AchievementManager.instance.DATA.GetData(i).ISACHIEVE)
            {
                UpdateAchievementDrawer(i);
            }
            #endregion
        }
    }

    //오버레이 객체 설정
    Image SetOverlay(Image img) {
        img.sprite = OverlayImg;
        img.color = new Color(0, 0, 0, 0.4f);
        return img;
    }

    //텍스트 객체 설정
    Text SetText(Text text)
    {
        //객체 정보 설정
        text.font = fontData;
        text.fontStyle = FontStyle.Bold;
        text.fontSize = 40;
        text.alignment = TextAnchor.MiddleCenter;
        text.color = Color.grey;

        //객체 위치값 설정
        text.rectTransform.anchorMax = new Vector2(1, 1);
        text.rectTransform.anchorMin = new Vector2(0, 0);
        text.rectTransform.sizeDelta = new Vector2(0, 0);
        return text;

    }
    

}
