﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class AchievemetPopupScript : MonoBehaviour
{
    [SerializeField] private Text Title;
    [SerializeField] private Text Msg;
    
    //AchievementManager에서 호출
    public void PrintAchieveText(string title, string msg)
    {
        Title.text = title;
        Msg.text = msg;
    }
}
